ARG PULL_TAG="dev"
FROM registry.gitlab.com/rocuisine/templates/docker/devcontainer/angular:${PULL_TAG}

COPY ./example/package.json .
COPY ./example/package-lock.json .

RUN sudo npm ci -g; \
  sudo apk add -no-cache curl bash; \
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended; \
  touch ~/.zshrc;

# Variables
variable "VERSION_TAG" {
  default=""
}

variable "DOCKER_TAG" {
  default = "local"
}

# Variables for local builds
variable "CI_COMMIT_SHORT_SHA" {
  default = ""
}

variable "CI_REGISTRY" {
  default = "registry.gitlab.com"
}

variable "PUBLIC_PATH" {
  default = "rocuisine"
}

# Groups
group "default" {
  targets = [
    "angular_demo",
    "angular_demo-dev",
  ]
}

# Targets
target "angular_demo" {
  inherits = [
    "default-args",
  ]
  context = "./example"
  dockerfile = "./Dockerfile"
  tags = [
    "${CI_REGISTRY}/${PUBLIC_PATH}/angular_demo:${DOCKER_TAG}",
    notequal("", VERSION_TAG) ? "${CI_REGISTRY}/${PUBLIC_PATH}/angular_demo:${VERSION_TAG}": "",
    notequal("", CI_COMMIT_SHORT_SHA) ? "${CI_REGISTRY}/${PUBLIC_PATH}/angular_demo:${CI_COMMIT_SHORT_SHA}": "",
  ]
}

target "angular_demo-dev" {
  inherits = [
    "default-args",
  ]
  context = "./"
  dockerfile = "./devcontainer.Dockerfile"
  tags = [
    "${CI_REGISTRY}/${PUBLIC_PATH}/angular_demo/devcontainer:${DOCKER_TAG}",
    notequal("", VERSION_TAG) ? "${CI_REGISTRY}/${PUBLIC_PATH}/angular_demo/devcontainer:${VERSION_TAG}": "",
    notequal("", CI_COMMIT_SHORT_SHA) ? "${CI_REGISTRY}/${PUBLIC_PATH}/angular_demo/devcontainer:${CI_COMMIT_SHORT_SHA}": "",
  ]
}

target "default-args" {
  platforms = [
    "linux/amd64",
  ]
  args = {
    PULL_TAG = "dev",
  }
}

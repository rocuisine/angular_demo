# Development in Docker


## VSCode

- Vorraussetzungen: Dieses Repository, Docker (Desktop) und VSCode
- Repo in VSCode öffnen
- Es sollte nun vorgeschlagen werden die Remote Containers extension zu installieren (`ms-vscode-remote.remote-containers`); ansonsten manuell durchführen
- Klick auf das Remote Dev Icon ("><" grün, Ecke, unten, links); oder öffne die VSCode Commandline (STRG+Shift+P) und tippe `Remote-Containers:`
- Wähle `Open Folder in Container` aus und dann den Ordner dieses Projektes wählen

## Sources

- [Okteto](https://okteto.com)
